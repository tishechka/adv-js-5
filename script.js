class Card {
    constructor(post, users) {
        this.post = post;
        this.users = users;
        this.cardElement = this.createCardElement();
        this.attachDeleteEvent();
    }

    createCardElement() {
        const container = document.createElement("div");
        container.id = "news-feed";

        const card = document.createElement("div");
        card.className = "card";

        const title = document.createElement("h3");
        title.textContent = this.post.title;

        const text = document.createElement("p");
        text.textContent = this.post.body;

        const userId = this.post.userId;
        const user = this.users.find(user => user.id === userId);

        const userElement = document.createElement("p");
        if (user && user.name && user.email) {
            userElement.textContent = `${user.name} (${user.email})`;
        } else {
            userElement.textContent = "Unknown User";
        }

        const deleteBtn = document.createElement("span");
        deleteBtn.className = "delete-btn";
        deleteBtn.innerHTML = "&times;";

        card.appendChild(title);
        card.appendChild(text);
        card.appendChild(userElement);
        card.appendChild(deleteBtn);

        container.appendChild(card);

        document.body.appendChild(container);

        return card;
    }

    attachDeleteEvent() {
        this.cardElement.querySelector(".delete-btn").addEventListener("click", () => {
            this.deleteCard();
        });
    }

    deleteCard() {
        const postId = this.post.id;
        fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (response.ok) {
                    this.cardElement.remove();
                } else {
                    console.error('Failed to delete the post');
                }
            })
            .catch(error => {
                console.error('Error deleting post:', error);
            });
    }
}

Promise.all([
    fetch('https://ajax.test-danit.com/api/json/posts').then(response => response.json()),
    fetch('https://ajax.test-danit.com/api/json/users').then(response => response.json())
])
    .then(([posts, users]) => {
        posts.forEach(post => {
            new Card(post, users);
        });
    })
    .catch(error => {
        console.error('Error fetching data:', error);
    });
